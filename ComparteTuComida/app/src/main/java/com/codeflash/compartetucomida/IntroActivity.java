package com.codeflash.compartetucomida;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class IntroActivity extends AppCompatActivity {

    TextView txt_iniciar_sesion;
    TextView txt_registrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        txt_iniciar_sesion = (TextView) findViewById(R.id.txt_iniciar_sesion);
        txt_registrate     = (TextView) findViewById(R.id.txt_registrate);

        Typeface fond = Typeface.createFromAsset(getAssets(),"Roboto-Bold.ttf");
        txt_iniciar_sesion.setTypeface(fond);
        txt_registrate.setTypeface(fond);


    }
}
