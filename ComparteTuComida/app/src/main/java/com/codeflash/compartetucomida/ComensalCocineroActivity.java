package com.codeflash.compartetucomida;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ComensalCocineroActivity extends AppCompatActivity implements View.OnClickListener{


    TextView txt_cocinero;
    TextView txt_comensal;
    LinearLayout ll_comensal;
    LinearLayout ll_cocinero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comensal_cocinero);

        txt_cocinero = (TextView) findViewById(R.id.txt_cocinero);
        txt_comensal = (TextView) findViewById(R.id.txt_comensal);
        ll_comensal = (LinearLayout) findViewById(R.id.ll_comensal);
        ll_cocinero = (LinearLayout) findViewById(R.id.ll_cocinero);

        /**
         * Dando formato roboto negrita a texto
         */
        Typeface fond = Typeface.createFromAsset(getAssets(),"Roboto-Bold.ttf");
        txt_cocinero.setTypeface(fond);
        txt_comensal.setTypeface(fond);
        /**
         * Habilitar onclick layout
         */
         ll_comensal.setOnClickListener(this);
         ll_cocinero.setOnClickListener(this);
    }

    /**
     * Metodo para manejar funciones de los botones
     */
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.ll_comensal:

                Intent siguiente = new Intent(ComensalCocineroActivity.this,ListaPlatosActivity.class);
                startActivity(siguiente);

                break;
            case R.id.ll_cocinero:
                Intent coc = new Intent(ComensalCocineroActivity.this,NuevoPlatoActivity.class);
                startActivity(coc);
                break;

            default:
                break;

        }

    }
}
