package com.codeflash.compartetucomida;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * Created by david on 01/05/2017.
 */

public class PlatoAdapter extends ArrayAdapter<Plato> {

    Context context;
    int layoutResourceId;
    Plato platos[] = null;

    public PlatoAdapter(Context context, int layoutResourceId, Plato[] platos) {
        super(context, layoutResourceId, platos);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.platos = platos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        PlatoHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new PlatoHolder();

            holder.img_plato_comida = (ImageView)row.findViewById(R.id.img_plato_comida);
            holder.txt_nombre_plato = (TextView)row.findViewById(R.id.txt_nombre_plato);
            holder.rb_valoracion    = (RatingBar)row.findViewById(R.id.rb_valoracion);
            holder.txt_nombre_cocinero = (TextView)row.findViewById(R.id.txt_nombre_cocinero);
            holder.txt_precio          = (TextView)row.findViewById(R.id.txt_precio);
            holder.txt_porciones       = (TextView)row.findViewById(R.id.txt_porciones);
            holder.txt_hora_entrega    = (TextView)row.findViewById(R.id.txt_hora_entrega);

            row.setTag(holder);

        }
        else
        {
            holder = (PlatoHolder)row.getTag();
        }

        Plato plato = platos[position];

        holder.img_plato_comida.setImageResource(plato.img);
        holder.txt_nombre_plato.setText(plato.nombre);
        holder.rb_valoracion.setRating(plato.valoracion);
        holder.txt_nombre_cocinero.setText(plato.cocinero);
        String precio_s = String.valueOf(plato.precio);
        holder.txt_precio.setText(precio_s);
        holder.txt_porciones.setText(plato.porciones);
        holder.txt_hora_entrega.setText(plato.hora_entrega);

        return row;
    }

    static class PlatoHolder
    {
        ImageView img_plato_comida;
        TextView txt_nombre_plato;
        RatingBar rb_valoracion;
        TextView txt_nombre_cocinero;
        TextView txt_precio;
        TextView txt_porciones;
        TextView txt_hora_entrega;


    }
}