package com.codeflash.compartetucomida;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txt_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_logo = (TextView) findViewById(R.id.txt_logo);
        Typeface font  = Typeface.createFromAsset(getAssets(),"Roboto-Black.ttf");
        txt_logo.setTypeface(font);
    }






}
