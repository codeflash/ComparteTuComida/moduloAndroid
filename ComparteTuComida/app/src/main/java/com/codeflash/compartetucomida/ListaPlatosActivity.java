package com.codeflash.compartetucomida;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarBadge;
import com.roughike.bottombar.OnMenuTabClickListener;

public class ListaPlatosActivity extends AppCompatActivity {


    BottomBar bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_platos);
        /**
         * Opcines de navegacion del foother bottonBar
         */
        bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.menu_main, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {

                if(menuItemId == R.id.btn_platos ){

                    FramePlatos framePlatos = new FramePlatos();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, framePlatos).commit();

                }else if(menuItemId == R.id.btn_chat){


                    FrameChat frameChat = new FrameChat();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, frameChat).commit();

                }else if(menuItemId == R.id.btn_notificacion){

                    FrameNotificacion frameNotificacion  = new FrameNotificacion();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, frameNotificacion).commit();

                }else if(menuItemId == R.id.btn_yo){

                    FrameYo frameYo = new FrameYo();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame, frameYo).commit();

                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {

            }
        });
        /**
         * Color al cambiar de boton
         */
        /*
        bottomBar.mapColorForTab(0,"#d5382b");
        bottomBar.mapColorForTab(1,"#d5382b");
        bottomBar.mapColorForTab(2,"#d5382b");
        bottomBar.mapColorForTab(3,"#d5382b");
        */
        /**
         * Numero de notificaciones
         */
        BottomBarBadge unread;
        unread = bottomBar.makeBadgeForTabAt(2,"#918d8d",17);
        unread.show();
        /**
         * Personalizar el header de la aplicacion
         */
        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.activity_nombre_app, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        TextView txt_titulo_header = (TextView) viewActionBar.findViewById(R.id.txt_titulo_header);
        Typeface fond = Typeface.createFromAsset(getAssets(),"Roboto-Regular.ttf");
        txt_titulo_header.setTypeface(fond);
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);


    }




}
