package com.codeflash.compartetucomida;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

/**
 * Created by david on 03/05/2017.
 */

public class ContactoAdapter extends ArrayAdapter<Contacto> {

    Context context;
    int layoutResourceId;
    Contacto contactos[] = null;

    public ContactoAdapter(Context context, int layoutResourceId, Contacto[] contactos) {
        super(context, layoutResourceId, contactos);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.contactos = contactos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ContactoAdapter.ContactoHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ContactoAdapter.ContactoHolder();

            holder.img_contacto = (CircularImageView) row.findViewById(R.id.img_contacto);
            holder.txt_nombre_contacto = (TextView) row.findViewById(R.id.txt_nombre_contacto);
            holder.txt_nombre_plato_chat = (TextView) row.findViewById(R.id.txt_nombre_plato_chat);
            holder.txt_ultimo_mensaje = (TextView) row.findViewById(R.id.txt_ultimo_mensaje);

            row.setTag(holder);

        }
        else
        {
            holder = (ContactoAdapter.ContactoHolder)row.getTag();
        }

        Contacto contacto = contactos[position];

        holder.img_contacto.setImageResource(contacto.img);
        holder.txt_nombre_contacto.setText(contacto.nombre);
        holder.txt_nombre_plato_chat.setText(contacto.nombre_plato);
        holder.txt_ultimo_mensaje.setText(contacto.mensaje);

        return row;
    }
    static class ContactoHolder
    {
        CircularImageView img_contacto;
        TextView txt_nombre_contacto;
        TextView txt_nombre_plato_chat;
        TextView txt_ultimo_mensaje;
    }
}
