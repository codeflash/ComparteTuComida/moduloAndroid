package com.codeflash.compartetucomida;

/**
 * Created by david on 03/05/2017.
 */

public class Contacto {

    public String nombre;
    public String nombre_plato;
    public String mensaje;
    public int img;

    public Contacto(){
        super();
    }
    public Contacto(String nombre, String nombre_plato, String mensaje, int img){
        super();
        this.nombre       = nombre;
        this.nombre_plato = nombre_plato;
        this.mensaje      = mensaje;
        this.img          = img;
    }

}
