package com.codeflash.compartetucomida;

/**
 * Created by david on 01/05/2017.
 */

public class Plato {

    public String nombre;
    public float valoracion;
    public String cocinero;
    public double precio;
    public String porciones;
    public String hora_entrega;
    public int img;

    public Plato(){
        super();
    }

    public Plato(String nombre,float valoracion,String cocinero, double precio, String porciones, String hora_entrega,int img) {
        super();
        this.nombre = nombre;
        this.valoracion =valoracion;
        this.cocinero = cocinero;
        this.precio = precio;
        this.porciones = porciones;
        this.hora_entrega = hora_entrega;
        this.img = img;
    }

}