package com.codeflash.compartetucomida;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegistrarseActivity extends AppCompatActivity {

    EditText etxt_nombre_apellido;
    EditText etxt_correo_electronico;
    EditText etxt_contrasenia;
    EditText etxt_numero_celular;
    Button   btn_registrarse;
    TextView txt_pregunta;
    TextView txt_inicia_sesion;
    TextView txt_aqui;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        etxt_nombre_apellido      = (EditText) findViewById(R.id.etxt_nombre_apellido);
        etxt_correo_electronico   = (EditText) findViewById(R.id.etxt_correo_electronico);
        etxt_contrasenia          = (EditText) findViewById(R.id.etxt_contrasenia);
        etxt_numero_celular       = (EditText) findViewById(R.id.etxt_numero_celular);
        btn_registrarse           = (Button) findViewById(R.id.btn_registrarse);
        txt_pregunta              = (TextView) findViewById(R.id.txt_pregunta);
        txt_inicia_sesion         = (TextView) findViewById(R.id.txt_inicia_sesion);
        txt_aqui                  = (TextView) findViewById(R.id.txt_aqui);
        Typeface fond = Typeface.createFromAsset(getAssets(),"Roboto-Light.ttf");
        etxt_nombre_apellido.setTypeface(fond);
        etxt_correo_electronico.setTypeface(fond);
        etxt_contrasenia.setTypeface(fond);
        etxt_numero_celular.setTypeface(fond);
        btn_registrarse.setTypeface(fond);
        txt_pregunta.setTypeface(fond);
        txt_inicia_sesion.setTypeface(fond);
        txt_aqui.setTypeface(fond);



    }
}
