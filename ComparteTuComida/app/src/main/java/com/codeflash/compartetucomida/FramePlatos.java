package com.codeflash.compartetucomida;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by david on 27/04/2017.
 */

public class FramePlatos extends Fragment {

    private ListView lv_platos;
    EditText etxt_buscador_plato;
    ImageButton img_buscar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.frame_platos,container,false);

        Plato platos[] = new Plato[]
                {
                        new Plato("Pastel Papa",3,"David Llicahua Huamani",10.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",2,"David Llicahua Huamani",12.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",1,"David Llicahua Huamani",11.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",5,"David Llicahua Huamani",5.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",3,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",4,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",4,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",1,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",2,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",3,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",4,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno),
                        new Plato("Pastel Papa",5,"David Llicahua Huamani",6.00,"3/5","5:00pm-6:00pm",R.mipmap.plato_uno)
                };

        PlatoAdapter adapter = new PlatoAdapter(getActivity(),R.layout.lista_platos,platos);

        lv_platos = (ListView) v.findViewById(R.id.lv_platos);
        lv_platos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(),"Detalle del Plato", Toast.LENGTH_LONG).show();
            }
        });
        lv_platos.setAdapter(adapter);

        /**
         * Inicializar variables
         */
        etxt_buscador_plato = (EditText) v.findViewById(R.id.etxt_buscador_plato);
        img_buscar          = (ImageButton) v.findViewById(R.id.img_buscar);

        return v;

    }



}
