package com.codeflash.compartetucomida;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class NuevoPlatoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_plato);

        Toolbar toolbarPlato = (Toolbar) findViewById(R.id.my_toolbar_Plato);
        setSupportActionBar(toolbarPlato);
        getSupportActionBar().setTitle("Nuevo Plato");

        toolbarPlato.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }
}
