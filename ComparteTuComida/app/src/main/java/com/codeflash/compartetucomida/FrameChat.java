package com.codeflash.compartetucomida;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by david on 27/04/2017.
 */

public class FrameChat extends Fragment  {

    private ListView lv_contactos;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.frame_chat,container,false);

        Contacto contactos[] = new Contacto[]
                {
                        new Contacto("German Llicahua","Matasquita1","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita2","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquit3a","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita4","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita5","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita6","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita7","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita8","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita9","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita10","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita1","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita11","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita12","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                        new Contacto("German Llicahua","Matasquita15","Donde puedo ir a recoger el plato estaba delicioso",R.mipmap.avatar),
                };


        ContactoAdapter adapter = new ContactoAdapter(getActivity(),R.layout.lista_contacto,contactos);
        lv_contactos = (ListView) v.findViewById(R.id.lv_contactos);
        lv_contactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent siguiente = new Intent(getActivity(),ChatActivity.class);
                startActivity(siguiente);

            }
        });
        lv_contactos.setAdapter(adapter);

        return v;

    }

    }

