package com.codeflash.compartetucomida;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InicioSesionActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etxt_correo;
    EditText etxt_contrasenia;
    Button   btn_incia_sesion;
    TextView txt_pregunta;
    TextView txt_crea_cuenta;
    TextView txt_aqui;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);

        etxt_correo      = (EditText) findViewById(R.id.etxt_correo);
        etxt_contrasenia = (EditText) findViewById(R.id.etxt_contrasenia);
        btn_incia_sesion = (Button) findViewById(R.id.btn_incia_sesion);
        txt_pregunta     = (TextView) findViewById(R.id.txt_pregunta);
        txt_crea_cuenta  = (TextView) findViewById(R.id.txt_crea_cuenta);
        txt_aqui         = (TextView) findViewById(R.id.txt_aqui);
        /**
         * Formato roboto para la letra
         */
        Typeface fond = Typeface.createFromAsset(getAssets(),"Roboto-Light.ttf");
        etxt_correo.setTypeface(fond);
        etxt_contrasenia.setTypeface(fond);
        btn_incia_sesion.setTypeface(fond);
        txt_pregunta.setTypeface(fond);
        txt_crea_cuenta.setTypeface(fond);
        txt_aqui.setTypeface(fond);
        /**
         * Habilitando los botones para implementar sus acciones
         */
        btn_incia_sesion.setOnClickListener(this);

    }

    /**
     *Manejo de las acciones de los botones
     */
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_incia_sesion:
                    Intent siguiente = new Intent(InicioSesionActivity.this,ComensalCocineroActivity.class);
                    startActivity(siguiente);
                break;
            default:
                break;

        }


    }



}
