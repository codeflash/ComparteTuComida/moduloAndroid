package com.codeflash.compartetucomida;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

/**
 * Created by david on 03/05/2017.
 */

public class NotificacionAdapter extends ArrayAdapter<Notificacion> {

    Context context;
    int layoutResourceId;
    Notificacion notificaciones[] = null;

    public NotificacionAdapter(Context context, int layoutResourceId, Notificacion[] notificaciones) {
        super(context, layoutResourceId, notificaciones);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.notificaciones = notificaciones;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        NotificacionAdapter.NotificacionHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new NotificacionAdapter.NotificacionHolder();

            holder.img_contacto_notificacion = (CircularImageView) row.findViewById(R.id.img_contacto_notificacion);
            holder.txt_notificacion = (TextView) row.findViewById(R.id.txt_notificacion);

            row.setTag(holder);

        }
        else
        {
            holder = (NotificacionAdapter.NotificacionHolder)row.getTag();
        }

        Notificacion notificacion = notificaciones[position];

        holder.img_contacto_notificacion.setImageResource(notificacion.img);
        holder.txt_notificacion.setText(notificacion.notificacion);

        return row;
    }

    static class NotificacionHolder
    {
        CircularImageView img_contacto_notificacion;
        TextView txt_notificacion;

    }

}
