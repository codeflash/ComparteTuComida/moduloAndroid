package com.codeflash.compartetucomida;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by david on 27/04/2017.
 */

public class FrameNotificacion extends Fragment {

    ListView lv_notificacion;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.frame_notificacion,container,false);


        Notificacion notificaciones[] = new Notificacion[]
                {
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 5",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 6",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 7",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 8",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 9",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 10",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 11",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 12",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 13",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 14",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 15",R.mipmap.avatar),
                        new Notificacion("German Llicahua acaba de ordenar platos de comidad que son platos 16",R.mipmap.avatar)

                };


        NotificacionAdapter adapter = new NotificacionAdapter(getActivity(),R.layout.lista_notificacion,notificaciones);

        lv_notificacion = (ListView) v.findViewById(R.id.lv_notificacion);
        lv_notificacion.setAdapter(adapter);

        v.getAnimation();

        return v;

    }




}
